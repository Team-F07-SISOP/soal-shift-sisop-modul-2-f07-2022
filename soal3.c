#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

char *user() {
    return getenv("USER");
}

char *home() {
    return getenv("HOME");
}

char *pwd() {
    return getenv("PWD");
}

char *default_dir() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/", home());
    return dir;
}

char *darat() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/darat", home());
    return dir;
}

char *air() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/air", home());
    return dir;
}

char *animal() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/animal", home());
    return dir;
}

void check_child(pid_t pid) {
    if(pid < 0) {
        exit(1);
    }
}

void create_dir() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        execlp("/bin/mkdir", "mkdir", "-p", darat(), NULL);
        perror("file modul2/darat gagal dibuat");
    } else {
        while ((wait(&status)) > 0);
        sleep(3);

        execlp("/bin/mkdir", "mkdir", "-p", air(), NULL);
        perror("file modul2/air gagal dibuat");
    }
}

void extract() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        char *args[] = {"unzip", "-o", "animal.zip", "-d", default_dir(), NULL};
        execvp("/bin/unzip", args);
    } else {
        while ((wait(&status)) > 0);
        //diem aja 
        execlp("/bin/echo", "echo", NULL);
    }
}

void filter_pic() {
    pid_t pid, pid2;

    (pid = fork()) && (pid2 = fork());

    int status;

    if(pid == 0) {
        char p[200];
        sprintf(p, "mv %s/*darat* %s", animal(), darat());
            
        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else if(pid2 == 0){
        char p[200];
        sprintf(p, "mv %s/*air* %s", animal(), air());

        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else {
        while ((wait(&status)) > 0);
        execlp("/bin/rm", "rm", "-rf", animal(), NULL);
    }
}

void remove_bird() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        char p[200];
        sprintf(p, "rm %s/*bird*", darat());
        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else {
        while ((wait(&status)) > 0);
        //diem aja 
        execlp("/bin/echo", "echo", NULL);
    }
}

char* permissions(char *file){
    struct stat st;
    char w, x, r;

    if(stat(file, &st) == 0){
        mode_t perm = st.st_mode;
        r = (perm & S_IRUSR) ? 'r' : '-';
        w = (perm & S_IWUSR) ? 'w' : '-';
        x = (perm & S_IXUSR) ? 'x' : '-';  
    }

    char *permit;
    permit = malloc(sizeof(char)*100);

    sprintf(permit, "%s_", user());

    if(r == 114) {
        strncat(permit, &r, 1);
    }

    if(w == 119) {
        strncat(permit, &w, 1);
    }

    if(x == 120) {
        strncat(permit, &x, 1);
    }

    return permit;
}

void handle_txt() {
    // ls | awk '{printf "uid_permission_%s\n", $1}' >> list.txt
    // 2 8 
    struct dirent *de;

    char *list[50];

    DIR *d = opendir(air());
    if (d == NULL) {
        printf("Could not open current directory");
        exit(1);
    }

    int len = 0;
    char *x;
    char *f;
    char *l;
    while ((de = readdir(d)) != NULL) {
        if(strcmp(de->d_name,"..") != 0 && strcmp(de->d_name,".") != 0) {
            x = (char *)malloc(strlen(de->d_name) + 50);
            f = (char *)malloc(strlen(de->d_name) + 100);
            l = (char *)malloc(strlen(de->d_name) + 100);

            //x = nama file
            //f = directory/namafile
            //l = uid_permissions_file
            strcpy(x,de->d_name);
            
            sprintf(f, "%s/%s", air(), x);

            sprintf(l, "%s_%s", permissions(f), x);

            list[len++] = l;
        }
    }

    char *txt_dir;
    txt_dir = malloc(sizeof(char)*100);
    sprintf(txt_dir, "%s/list.txt", air());

    FILE *txt = fopen(txt_dir, "w");
    if(txt == NULL) {
        printf("Failed in handling file txt\n");
        exit(1);
    }

    for(int i = 0; i < len; ++i) {
        fprintf(txt, "%s\n", list[i]);
    }

    fclose(txt);
}

int main() {
    int status;

    pid_t child_a;
    child_a = fork();
    check_child(child_a);

    if(child_a == 0) {
        create_dir();
    } else {
        while ((wait(&status)) > 0);
        kill(child_a, SIGKILL);
        waitpid(child_a, &status, 0); // harvest the zombie
    }

    pid_t child_b;
    child_b = fork();
    check_child(child_b);

    if(child_b == 0) {
        extract();
    } else {
        while ((wait(&status)) > 0);
        kill(child_b, SIGKILL);
        waitpid(child_b, &status, 0);
    }

    pid_t child_c;
    child_c = fork();
    check_child(child_c);

    if(child_c == 0) {
        filter_pic();
    } else {
        while ((wait(&status)) > 0);
        kill(child_c, SIGKILL);
        waitpid(child_c, &status, 0);
    }

    pid_t child_d;
    child_d = fork();
    check_child(child_d);

    if(child_d == 0) {
        remove_bird();
    } else {
        while ((wait(&status)) > 0);
        kill(child_d, SIGKILL);
        waitpid(child_d, &status, 0);
    }

    pid_t child_e;
    child_e = fork();
    check_child(child_e);

    if(child_e == 0) {
        handle_txt();
    } else {
        while ((wait(&status)) > 0);
        kill(child_e, SIGKILL);
        waitpid(child_e, &status, 0);
    }

    return 0;
}