## Anggota Kelompok

Mohammad Fadhil Rasyidin Parinduri // 5025201131

Farrel Emerson // 5025201082

Samuel // 5025201187

### Soal Shift
- [link](https://docs.google.com/document/d/1qWo-VT3bYx8t9Bnpbq_h8Mpn9FpBM3t4T6SWX3vn9RI/edit?usp=sharing)


Kendala:
1. Secara pengetahuan masih kurang mumpuni
2. Sulit menelusuri child process
3. Sulit menentukan task mana yang perlu dikerjakan terlebih dahulu


# Laporan Resmi
### Soal Nomor 1
- [soal1.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-2-f07-2022/-/blob/main/soal1.c)

#### Soal 1a
##### Source Code 1a
```c
int main(){
  int status;
  pid_t wpid;

  if (fork() == 0) {
    char *CharaDown[] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "Chara.zip", NULL};
    execv("/usr/bin/wget", CharaDown);
  }

  wait(NULL);

  if (fork() == 0) {
    char *WeapDown[] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "Weap.zip", NULL};
    execv("/usr/bin/wget", WeapDown);
  }

  wait(NULL);

  if (fork() == 0) {
    char *UnzipChara[] = {"unzip", "Chara.zip", NULL};
    execv("/usr/bin/unzip", UnzipChara);
  }

  if (fork() == 0) {
    char *UnzipWeap[] = {"unzip", "Weap.zip", NULL};
    execv("/usr/bin/unzip", UnzipWeap);
  }

  while ((wait(&status)) > 0);

  if (fork() == 0){
    char *rmChara[] = {"rm", "-f", "Chara.zip", NULL};
    execv("/usr/bin/rm", rmChara);
  }
  
  if (fork() == 0){
    char *rmWeap[] = {"rm", "-f", "Weap.zip", NULL};
    execv("/usr/bin/rm", rmWeap);
  }

  if (fork() == 0){
    char *DirGacha[] = {"mkdir", "gacha_gacha", NULL};
    execv("/usr/bin/mkdir", DirGacha);
  }

```
##### Cara pengerjaan
1. Menggunakan `wget` untuk mendownload file zip character dan weapon
2. Menggunakan `unzip` untuk mengunzip kedua file dan `mkdir` untuk membuat folder gacha_gacha
3. Setiap execv menggunakan `fork()` untuk membuat child process

#### Soal 1bcd

##### getDir
```c

void getDir(FILE *list[], char dirPath[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;
    char pathTemp[256];

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
        {
            sprintf(pathTemp, "%s%s", dirPath, dir->d_name);
            list[i] = fopen(pathTemp, "r+");
            i++;
        }
    }

    closedir(d);
    return;
}
```
Function ini untuk memindahkan nama nama file yang ada difolder ke array strings yang nanti akan dipakai untuk menggacha

Setelah ini, permainan gacha akan disapkan, pertama dengan mendeklarasi beberapa variabel yang dibutuhkan untuk permainan ini, yaitu gachaCounter, primogems, tempFileString, tempFileStringArrCounter,
startTime, characterFileTotal, dan weaponFileTotal. Deklarasi dari variabel-variabel tersebut adalah sebagai berikut:
```c

    unsigned int gachaCounter = 0;
    int primogems = 79000;
    char *tempFileString[10];
    short tempFileStringArrCounter = 0;
    time_t startTime = time(NULL);
```

##### countFiles
```c
int countFiles(char path[])
{
    int count = 0;
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir)
        return -1;

    while ((dp = readdir(dir)) != NULL)
    { // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            count++;
        }
    }

    // Close directory stream
    closedir(dir);
    return count;
}
```
Function ini untuk mengecek banyak file pada folder untuk function gacha
Setelah itu kita menggunakan function yang nanti akan digunakan untuk function gacha
Mulainya game gacha dilakukan dengan memasuki while loop. While loop tersebut akan selesai ketika antara primogems sudah tidak mencukupi untuk melakukan sebuah gacha, atau tiga jam telah berlalu. Jika
diantara kedua kondisi tersebut belum terjadi, maka setiap detik akan melakukan gacha. Logaritma gacha tersimpan di dalam function gachaAlgorithm yang akan memanggil function __gachaAlgorithm untuk
mengurusi bagian-bagian yang berurusan dengan pemilihan gachanya. Function gachaAlgorithm lebih mengurusi penulisan hasil dari gacha ke txt file dan pembuatan folder-folder. Kode gachaAlgorithm adalah
sebagai berikut:
##### gachaAlgorithm
```c
void gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], unsigned int *gachaCounter, int *primogems, char *tempFileString[], time_t currTime, short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal)
{
    (*primogems) -= gachaCost;
    (*gachaCounter)++;

    __gachaAlgorithm(characterList, weaponsList, tempFileString, tempFileStringArrCounter, weaponFileTotal, characterFileTotal, gachaCounter, primogems);

    if ((*gachaCounter) % 10 == 0)
    {

        char txtName[100];
        struct tm timeStruct = *localtime(&currTime);

        sprintf(txtName, "./gacha_gacha/%d_%d_%d_gacha_%d.txt", timeStruct.tm_hour, timeStruct.tm_min, timeStruct.tm_sec, *gachaCounter);

        FILE *writer = fopen(txtName, "w+");

        for (int i = 0; i < 10; i++)
        {
            fprintf(writer, "%s\n", tempFileString[i]);
            strcpy(tempFileString[i], "");
        }

        *tempFileStringArrCounter = 0;
        fclose(writer);
    }

    if ((*gachaCounter) % 90 == 0)
    {
        char dirName[100];
        char *dirList[50];
        FILE *txtList[100];
        char tempMoveDir[100];

        for (int i = 0; i < 50; i++)
        {
            dirList[i] = (char *)malloc(sizeof(char) * 50);
        }

        sprintf(dirName, "./gacha_gacha/total_gacha_%d", *gachaCounter);
        makeDirectory(dirName);

        getDirStringWithFilter("./gacha_gacha/", dirList, ".txt");

        for (int i = 0; strcmp("FINISHED", dirList[i]) != 0; i++)
        {
            sprintf(tempMoveDir, "./gacha_gacha/%s", dirList[i]);
            mvFile(tempMoveDir, dirName);
        }

        for (int i = 0; i < 50; i++)
        {
            free(dirList[i]);
        }
    }

    return;
}
```
Jika gachaCounter dapat dibagi 10, maka semua isi dari tempFileString akan ditulis ke dalam txt file sesuai format. Selain itu, ketika gachaCounter dapat dibagi 90, directory baru akan dibuat
sesuai ketentuan soal. Selain pembuatan directory baru, semua txt file juga akan dipindahkan ke dalam folder tersebut. Hal tersebut dilakukan dengan dua function getDirStringWithFilter dan
mvFile. Function kedua merupakan function wrapper untuk mv yang cukup sederhana. getDirStringWithFilter merupakan sebuah function yang mirip dengan getDir, tetapi function tersebut akan
memberi output berupa string dan akan memfilter hasil sesuai string filter yang dimasukkan ke dalam argumennya. Kodenya adalah sebagai berikut:
##### mvFile
```c
void mvFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mv", filePath, destPath, NULL};
        execv("/usr/bin/mv", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

```


```c
void getDirStringWithFilter(char dirPath[], char *dirList[], char filterPhrase[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strstr(dir->d_name, filterPhrase))
        {
            strcpy(dirList[i], dir->d_name);
            i++;
        }
    }

    strcpy(dirList[i], "FINISHED");

    closedir(d);
    return;
}
```
String yang ditulis ke dalam txt file tersebut akan berasal dari array of string di dalam tempFileString. Array tersebut berisi dengan hasil-hasil gacha dengan kapasitas sepuluh. Pengisian dari array
tersebut menggunakan function __gachaAlgorithm yang akan menggunakan function random untuk memilih array index secara acak untuk memilih antara weapons atau characters. Karena data weapons
dan characters tersimpan di dalam bentuk JSON, maka diperlukan untuk mem-parse data JSON terlebih dahulu. Setelah dibaca, data tersebut akan ditulis ke dalam tempFileString sesuai dengan format.
Kode dari function ini adalah sebagai berikut:
```c
void __gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], char *tempFileString[], short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal, unsigned int *gachaCounter, int *primogems)
{
    if ((*tempFileStringArrCounter) % 2 == 0)
    {

        // weapons
        long RNG = random() % weaponFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, weaponsList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_weapons_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }
    else
    {
        // characters
        long RNG = random() % characterFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, characterList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_characters_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }

    (*tempFileStringArrCounter)++;
    return;
}
```
Ketika primogems sudah tidak mencukupi atau tiga jam telah berjalan, akan di-zip semua hasil dari gacha-gacha menjadi sebuah zip bernama not_safe_for_wibu dengan password satuduatiga. Hal ini
diurusi dengan function gachaZip yang berisi sebagai berikut:
```c
void gachaZip()
{
    if (fork() == 0)
    {
        char *arguments[] = {"zip", "--password", "satuduatiga", "not_safe_for_wibu.zip", "-r", "./gacha_gacha/", "-m", NULL};
        execv("/usr/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }

    int status;
    while (wait(&status) >= 0)
        ;

    return;
}
```
untuk membuat daemon menggunakan function create daemon dengan fungsi berikut
##### createDaemon
```c
pid_t createDaemon()
{
    pid_t pid, sid;

    pid = fork();
    if (pid > 0)
    {
        printf("Parent process akan diberhentikan\n");
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0)
        exit(EXIT_FAILURE);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    return sid;
}

```
jika sudah selesai file akan dihapus dengan rmFile dan rmDir;
```c
void rmFile(char fileName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rm", "-r", fileName, NULL};
        execv("/usr/bin/rm", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void rmDir(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rmdir", dirName, NULL};
        execv("/usr/bin/rmdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}
```

### Soal Nomor 2
- [soal2.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-2-f07-2022/-/blob/main/soal2.c)

#### Soal 2a
##### Source Code 2a
```c
void Execute(char *argv[], char command[]) {
	child_id = CreateChildProcess();
	
	if (child_id == 0) {
		execv(command, argv);
	}
	while ((wait(&status)) > 0);
}

void CreateFolder(char folder_name[]) {
	char *argv[] = {"mkdir", "-p", folder_name, NULL};
	Execute(argv, "/usr/bin/mkdir");
}

void ExtractFiles() {
	CreateFolder("shift2");
	
  	char *argv[] = {"unzip", "drakor.zip","-d","shift2/drakor", NULL};
  	Execute(argv, "/usr/bin/unzip");  	
}

void RemoveFile(char file[]) {
    	char *argv[] = {"remove", "-r", file, NULL};
    	Execute(argv, "/usr/bin/rm");
}

void DeleteFolders() {
	struct dirent *dp;
	DIR *dir = opendir("./");
	
	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{
        	struct stat fs;
        		
		    int r = stat(dp->d_name,&fs);
    		if( r==-1 ) {
        		fprintf(stderr,"File error\n");
        		exit(1);
    		}
    			
    		//Delete non-PNG
    		if(fs.st_mode != 0100664) {
    			RemoveFile(dp->d_name);
    		}
       	}
   	}
    closedir(dir);
}
```

##### Cara Pengerjaan
Tujuan: Extract `drakor.zip` ke dalam directory path bernama `drakor` dan membedakan file penting dan tidak penting kemudian hapus file yang tidak penting.
1. mengikuti kaidah modular, kami membuat function untuk membuat folder, execute, dan extract files
2. `Execute` berfungsi untuk create child process dan jika `child_id == 0` maka akan menjalankan execv dengan parameter command dan argv
3. `CreateFolder` berfungsi untuk membuat folder dengan menyetor fungsi `mkdir` dengan parameter `folder_name` yaitu nama folder yang diinginkan.
4. `ExtractFiles` berfungsi untuk sesuai namanya, extract files. dilakukan dengan cara menyimpan `unzip` dalam `argv` kemudian panggil Execute untuk menjalankannya.
5. `DeleteFolders` berfungsi untuk cek satu persatu file dan folder yang ada di dalam folder drakor. Jika ternyata tipe filenya bukan PNG (fs.st_mode-nya bukan 0100664), hapus foldernya.
6. `RemoveFile` berfungsi untuk parameter "-r" memperbolehkan menghapus directory dan file adalah nama file yang ingin dihapus. Kemudaian jalankan fungsi Execute() dengan parameter argv dan `/usr/bin/rm`.
7. keempat fungsi tersebut akan dipanggil semua pada main.

#### Soal 2b
##### Source Code 2b
```c
void ParseString(char *d, char s[], int *i) {
	int j = 0;
	while(s[*i] != ';' && s[*i] != '_' && s[*i] != '.') {
		d[j] = s[*i];
		j++;
		(*i)++;
	}
	d[j] = '\0';
}

void ProcessPhoto(char name[]) {
	int i = 0;
	while(name[i] != '.') {
		char title[35], year[10], category[20];
		
		ParseString(title, name, &i);
		i++;
		
		ParseString(year, name, &i);
		i++;
		
		ParseString(category, name, &i);
		if(name[i] == '_') {
			i++;
		}
		CreateFolder(category);
		CopyFile(name, category);
		ChangeWorkingDirectory(category);
		RenameFile(name, title);
		InsertData(year, title);
		ChangeWorkingDirectory("../");	
	}
	RemoveFile(name);
}

void CreateCategory() {
	struct dirent *dp;
	DIR *dir = opendir("./");
	
	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{	
        	ProcessPhoto(dp->d_name);
        }
    }
    closedir(dir);
}
```

##### Cara Pengerjaan
Tujuan: kategorikan poster drama korea sesuai jenisnya dalam folder untuk setiap jenis drama korea dalam `drakor.zip`
1. `ParseString` berfungsi untuk mendapatkan huruf atau kata yang ada dalam suatu variabel string yang dimulai dari indeks i hingga menemui salah satu dari ';' atau '_' atau '.'. Huruf atau kata yang telah di-parse tadi diakhiri dengan '\0' untuk menandakan akhir dari string.
2. `ProcessPhoto` berfungsi untuk pertama-tama, menginisialisasi variabel i untuk menunjukkan indeks string dari nama file. kemudian, memisahkan nama file menjadi title, year, dan category-nya dengan menggunakan fungsi ParseString(). terakhir, Jika dalam foto itu terdapat dua poster, maka pemisahan nama file diatas dijalankan lagi karena masih belum menemui '.'.
3. `CreateCategory` berfungsi untuk cek satu persatu file yang ada di dalam folder drakor dan jalankan fungsi ProcessPhoto dengan nama file sebagai parameter
4. tiga fungsi tersebut akan dipanggil di main.

#### Soal 2c
##### Source Code 2c
```c
void CopyFile(char source[], char destination[]) {
  	char *argv[] = {"cp", source, destination, NULL};
  	Execute(argv, "/bin/cp"); 
}

void InsertData(char year[], char title[]) {
	FILE *file = fopen("temp.txt" , "a");
	
	fprintf(file, "%s %s\n", year, title);
	
	fclose(file);
}

void RenameFile(char source[], char destination[]) {
  	char *argv[] = {"mv", source, destination, NULL};
  	Execute(argv, "/usr/bin/mv");	
}
```

##### Cara Pengerjaan
Tujuan: Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
1. `CopyFile` berfungsi untuk copy parameter source, file yang ingin dicopy dan destination adalah tujuannya. Kemudian jalankan fungsi Execute() dengan parameter argv dan "/bin/cp".
2. `InsertData` berfungsi untuk menyimpan informasi tentang suatu film dari masing - masing kategori. Informasi yang disimpan adalah tahun rilis dan judulnya.
3. `RenameFile` berfungsi untuk rename parameter source, file yang ingin direname dan destination adalah nama barunya. Jalankan fungsi Execute() dengan parameter argv dan "/usr/bin/mv".
4. panggil ketiga fungsi tersebut di main.

#### Soal 2d
##### Cara Pengerjaan
Tujuan: Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai.
> Soal 2d sudah sekaligus diselesaikan pada jawaban bagian 2b dan 2c.

#### Soal 2e
##### Source Code 2e
```c
void ProcessCategoryData() {
	struct dirent *dp;
	DIR *dir = opendir("./");
	
	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
       	if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{
        	ChangeWorkingDirectory(dp->d_name);
        	SortData();
        	StoreData(dp->d_name);
        	ChangeWorkingDirectory("../");
        }
    }
   	closedir(dir);
}

void SortData() { 	
    	FILE *file = fopen("sort.txt" , "w");
    	fclose(file);
    				
    	char *argv[] = {"sort", "-o", "sort.txt", "temp.txt", NULL};
    	Execute(argv, "/usr/bin/sort");

    	RemoveFile("temp.txt");
}

void StoreData(char category[]) {
	FILE *file = fopen("sort.txt" , "r");
	FILE *output = fopen("data.txt", "w");
	
	char year[10][10];
	char title[10][50];
	int i = 0;
	while(fscanf(file, "%s %s", year[i], title[i]) != EOF) {
		i++;
	}
	
	fprintf(output, "kategori : %s\n", category);
	for(int j = 0; j < i; j++) {
		fprintf(output, "\nnama : %s\nrilis : tahun %s\n", title[j], year[j]);
	}
	
	fclose(output);
	fclose(file);
	
	RemoveFile("sort.txt");
}
```

##### Cara Pengerjaan
Tujuan: Di setiap folder kategori drama korea buatlah sebuah file `data.txt` yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Sesuai dengan format yang dibutuhkan soal juga.
1. `ProcessCategoryData` berfungsi untuk cek setiap folder kategori, kemudian ganti working directory menjadi kategori tersebut. Lalu, jalankan fungsi SortData().
2. `SortData` berfungsi untuk membuat file `sort.txt` jika belum ada menggunakan fungsi fopen(). Kemudian, isi argv dengan output file `sort.txt` dan input file `temp.txt`. Karena dalam file temp.txt format nama adalah tahun rilis kemudian nama filmnya, yang diutamakan dalam sortir adalah tahun rilis. Jika tahun rilisnya sama, maka yang dijadikan acuan adalah nama filmnya. Hapus file `temp.txt`
3. `StoreData` berfungsi untuk baca setiap baris dari file `sort.txt` hingga EOF. Tahun rilis dan nama film di dalam sort.txt dimasukkan ke variabel year dan title. Kemudian tulis kategori apa yang sedang dibuka ke dalam file `data.txt`. Kemudian, tulis nama dan tahun rilis setiap film dalam kategori tersebut. Akhirnya, hapus file `sort.txt`

### Soal Nomor 3
- [soal3.c](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-2-f07-2022/-/blob/main/soal3.c)

#### Soal 3a
##### Source Code 3a
```c
char *home() {
    return getenv("HOME");
}

char *darat() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/darat", home());
    return dir;
}

char *air() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/air", home());
    return dir;
}

void create_dir() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        execlp("/bin/mkdir", "mkdir", "-p", darat(), NULL);
        perror("file modul2/darat gagal dibuat");
    } else {
        while ((wait(&status)) > 0);
        sleep(3);

        execlp("/bin/mkdir", "mkdir", "-p", air(), NULL);
        perror("file modul2/air gagal dibuat");
    }
}
```

##### Cara Pengerjaan
1. Melakukan fork untuk membuat directory file yang diminta (air dan darat)
2. Membuat folder darat terlebih dahulu dan check seandainya terjadi error saat pembuatan folder
3. `sleep(3)`, artinya program akan diam sementara untuk 3 detik, kemudian membuat folder air


#### Soal 3b
##### Source Code 3b
```c
char *default_dir() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/", home());
    return dir;
}

void extract() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        char *args[] = {"unzip", "-o", "animal.zip", "-d", default_dir(), NULL};
        execvp("/bin/unzip", args);
    } else {
        while ((wait(&status)) > 0);
        //diem aja 
        execlp("/bin/echo", "echo", NULL);
    }
}
```
##### Cara Pengerjaan
1. Melakukan fork pada function extract
2. Melakukan unzip pada child process dan menaruh hasil unzip nya di folder yang sudah ditentukan pada `char *default_dir()`
3. Pada parent process tidak perlu dilakukan apapun, jadi lakukan saja echo kosong


#### Soal 3c
##### Source Code 3c
```c
char *animal() {
    char *dir;
    dir = malloc(sizeof(char)*50);
    sprintf(dir,"%s/modul2/animal", home());
    return dir;
}

void filter_pic() {
    pid_t pid, pid2;

    (pid = fork()) && (pid2 = fork());

    int status;

    if(pid == 0) {
        char p[200];
        sprintf(p, "mv %s/*darat* %s", animal(), darat());
            
        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else if(pid2 == 0){
        char p[200];
        sprintf(p, "mv %s/*air* %s", animal(), air());

        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else {
        while ((wait(&status)) > 0);
        execlp("/bin/rm", "rm", "-rf", animal(), NULL);
    }
}
```
##### Cara Pengerjaan
1. Melakukan fork pada 2 pid yang ada, karena akan dilakukan 2 process yang tidak mementingkan urutan
2. Untuk child proses pertama, lakukan `mv HOME/modul2/animal/*darat*`, artinya jika pada nama file nya terdapat unsur darat, pindahkan ke file darat
3. Untuk child proses kedua, lakukan `mv HOME/modul2/animal/*air*`, artinya jika pada nama file nya terdapat unsur air, pindahkan ke file air
4. Jika kedua proses sudah selesai, hapus file animal


#### Soal 3d
##### Source Code 3d
```c
void remove_bird() {
    pid_t pid = fork();
    int status;

    if(pid == 0) {
        char p[200];
        sprintf(p, "rm %s/*bird*", darat());
        char *args[] = {"bash", "-c", p, NULL};
        execvp("/bin/bash", args);
    } else {
        while ((wait(&status)) > 0);
        //diem aja 
        execlp("/bin/echo", "echo", NULL);
    }
}
```
##### Cara Pengerjaan
1. Melakukan fork 
2. Pada bagian child, lakukan `rm *bird*` pada folder darat, seperti yang dilakukan pada bagian 3c sebelumnya
3. Jika proses sudah selesai, tidak perlu melakukan apa - apa di parent


#### Soal 3e
##### Source Code 3e
```c
char* permissions(char *file){
    struct stat st;
    char w, x, r;

    if(stat(file, &st) == 0){
        mode_t perm = st.st_mode;
        r = (perm & S_IRUSR) ? 'r' : '-';
        w = (perm & S_IWUSR) ? 'w' : '-';
        x = (perm & S_IXUSR) ? 'x' : '-';  
    }

    char *permit;
    permit = malloc(sizeof(char)*100);

    sprintf(permit, "%s_", user());

    if(r == 114) {
        strncat(permit, &r, 1);
    }

    if(w == 119) {
        strncat(permit, &w, 1);
    }

    if(x == 120) {
        strncat(permit, &x, 1);
    }

    return permit;
}
```
##### Penjelasan
Pada bagian ini digunakan untuk mencari permissions yang dimiliki sebuah file. Pertama adalah mengecek permissionnya, apakah mengandung r(read), w(write), ataupun x(exec). Lalu buatlah string dengan nama variable `permit` yang akan membentuk `UID_` pada format file. Kemudian bagian if else adalah untuk mengecek hasil dari char r,w,x. Jika salah satunya mengandung char nya (dinyatakan dalam ASCII), lakukan strncat.
Contoh : jika sebuah file mempunyai permission r dan w, maka namanya akan menjadi UID_rw.


```c
void handle_txt() {
    // ls | awk '{printf "uid_permission_%s\n", $1}' >> list.txt
    // 2 8 
    struct dirent *de;

    char *list[50];

    DIR *d = opendir(air());
    if (d == NULL) {
        printf("Could not open current directory");
        exit(1);
    }

    int len = 0;
    char *x;
    char *f;
    char *l;
    while ((de = readdir(d)) != NULL) {
        if(strcmp(de->d_name,"..") != 0 && strcmp(de->d_name,".") != 0) {
            x = (char *)malloc(strlen(de->d_name) + 50);
            f = (char *)malloc(strlen(de->d_name) + 100);
            l = (char *)malloc(strlen(de->d_name) + 100);

            //x = nama file
            //f = directory/namafile
            //l = uid_permissions_file
            strcpy(x,de->d_name);
            
            sprintf(f, "%s/%s", air(), x);

            sprintf(l, "%s_%s", permissions(f), x);

            list[len++] = l;
        }
    }

    char *txt_dir;
    txt_dir = malloc(sizeof(char)*100);
    sprintf(txt_dir, "%s/list.txt", air());

    FILE *txt = fopen(txt_dir, "w");
    if(txt == NULL) {
        printf("Failed in handling file txt\n");
        exit(1);
    }

    for(int i = 0; i < len; ++i) {
        fprintf(txt, "%s\n", list[i]);
    }

    fclose(txt);
}
```
##### Cara Pengerjaan
1. Mengecek directory nya apakah ada atau tidak
2. Jika ada, untuk setiap file yang ada dalam folder, masukkan nama file yang ada ke 3 variable x,f,l. Dimana `x` untuk nama file, `f` untuk membuat directory nama file, `l` untuk format nama yang lengkap yang diminta soal. Dan masukkan ke array of string bernama list
3. Lalu masukkan nama file yang ada dalam array of string di list ke file bernama list.txt, lalu jangan lupa tutup file .txt nya.
