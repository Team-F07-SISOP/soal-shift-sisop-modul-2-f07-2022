#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <json-c/json.h>
#include <sys/stat.h>
#define gachaCost 160



void rmFile(char fileName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rm", "-r", fileName, NULL};
        execv("/usr/bin/rm", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void rmDir(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"rmdir", dirName, NULL};
        execv("/usr/bin/rmdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}


void mvFile(char filePath[], char destPath[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mv", filePath, destPath, NULL};
        execv("/usr/bin/mv", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}

void makeDirectory(char dirName[])
{
    if (fork() == 0)
    {
        char *arguments[] = {"mkdir", "-p", dirName, NULL};
        execv("/usr/bin/mkdir", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0)
        ;
    return;
}


void getDir(FILE *list[], char dirPath[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;
    char pathTemp[256];

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
        {
            sprintf(pathTemp, "%s%s", dirPath, dir->d_name);
            list[i] = fopen(pathTemp, "r+");
            i++;
        }
    }

    closedir(d);
    return;
}

void getDirStringWithFilter(char dirPath[], char *dirList[], char filterPhrase[])
{
    DIR *d;
    struct dirent *dir;
    int i = 0;

    d = opendir(dirPath);

    while ((dir = readdir(d)) != NULL)
    {
        if (strstr(dir->d_name, filterPhrase))
        {
            strcpy(dirList[i], dir->d_name);
            i++;
        }
    }

    strcpy(dirList[i], "FINISHED");

    closedir(d);
    return;
}


void __gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], char *tempFileString[], short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal, unsigned int *gachaCounter, int *primogems)
{
    if ((*tempFileStringArrCounter) % 2 == 0)
    {

        // weapons
        long RNG = random() % weaponFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, weaponsList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_weapons_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }
    else
    {
        // characters
        long RNG = random() % characterFileTotal;

        char buffer[8192];
        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        fread(buffer, 8192, 1, characterList[RNG]);
        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(tempFileString[*tempFileStringArrCounter], "%u_characters_%d_%s_%d", *gachaCounter, json_object_get_int(rarity), json_object_get_string(name), *primogems);
    }

    (*tempFileStringArrCounter)++;
    return;
}

void gachaAlgorithm(FILE *characterList[], FILE *weaponsList[], unsigned int *gachaCounter, int *primogems, char *tempFileString[], time_t currTime, short *tempFileStringArrCounter, int weaponFileTotal, int characterFileTotal)
{
    (*primogems) -= gachaCost;
    (*gachaCounter)++;

    __gachaAlgorithm(characterList, weaponsList, tempFileString, tempFileStringArrCounter, weaponFileTotal, characterFileTotal, gachaCounter, primogems);

    if ((*gachaCounter) % 10 == 0)
    {

        char txtName[100];
        struct tm timeStruct = *localtime(&currTime);

        sprintf(txtName, "./gacha_gacha/%d_%d_%d_gacha_%d.txt", timeStruct.tm_hour, timeStruct.tm_min, timeStruct.tm_sec, *gachaCounter);

        FILE *writer = fopen(txtName, "w+");

        for (int i = 0; i < 10; i++)
        {
            fprintf(writer, "%s\n", tempFileString[i]);
            strcpy(tempFileString[i], "");
        }

        *tempFileStringArrCounter = 0;
        fclose(writer);
    }

    if ((*gachaCounter) % 90 == 0)
    {
        char dirName[100];
        char *dirList[50];
        FILE *txtList[100];
        char tempMoveDir[100];

        for (int i = 0; i < 50; i++)
        {
            dirList[i] = (char *)malloc(sizeof(char) * 50);
        }

        sprintf(dirName, "./gacha_gacha/total_gacha_%d", *gachaCounter);
        makeDirectory(dirName);

        getDirStringWithFilter("./gacha_gacha/", dirList, ".txt");

        for (int i = 0; strcmp("FINISHED", dirList[i]) != 0; i++)
        {
            sprintf(tempMoveDir, "./gacha_gacha/%s", dirList[i]);
            mvFile(tempMoveDir, dirName);
        }

        for (int i = 0; i < 50; i++)
        {
            free(dirList[i]);
        }
    }

    return;
}

int countFiles(char path[])
{
    int count = 0;
    struct dirent *dp;
    DIR *dir = opendir(path);

    // Unable to open directory stream
    if (!dir)
        return -1;

    while ((dp = readdir(dir)) != NULL)
    { // Iterate every file
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            count++;
        }
    }

    // Close directory stream
    closedir(dir);
    return count;
}


void gachaZip()
{
    if (fork() == 0)
    {
        char *arguments[] = {"zip", "--password", "satuduatiga", "not_safe_for_wibu.zip", "-r", "./gacha_gacha/", "-m", NULL};
        execv("/usr/bin/zip", arguments);
        exit(EXIT_SUCCESS);
    }

    int status;
    while (wait(&status) >= 0)
        ;

    return;
}

pid_t createDaemon()
{
    pid_t pid, sid;

    pid = fork();
    if (pid > 0)
    {
        printf("Parent process akan diberhentikan\n");
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0)
        exit(EXIT_FAILURE);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    return sid;
}




int main(){
  int status;
  pid_t wpid;

  if (fork() == 0) {
    char *CharaDown[] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "Chara.zip", NULL};
    execv("/usr/bin/wget", CharaDown);
  }

  wait(NULL);

  if (fork() == 0) {
    char *WeapDown[] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "Weap.zip", NULL};
    execv("/usr/bin/wget", WeapDown);
  }

  wait(NULL);

  if (fork() == 0) {
    char *UnzipChara[] = {"unzip", "Chara.zip", NULL};
    execv("/usr/bin/unzip", UnzipChara);
  }

  if (fork() == 0) {
    char *UnzipWeap[] = {"unzip", "Weap.zip", NULL};
    execv("/usr/bin/unzip", UnzipWeap);
  }

  while ((wait(&status)) > 0);

  if (fork() == 0){
    char *rmChara[] = {"rm", "-f", "Chara.zip", NULL};
    execv("/usr/bin/rm", rmChara);
  }
  
  if (fork() == 0){
    char *rmWeap[] = {"rm", "-f", "Weap.zip", NULL};
    execv("/usr/bin/rm", rmWeap);
  }

  if (fork() == 0){
    char *DirGacha[] = {"mkdir", "gacha_gacha", NULL};
    execv("/usr/bin/mkdir", DirGacha);
  }

  while ((wpid = wait(&status)) > 0);

  pid_t processID = createDaemon();
    // Setelah ini, hanya daemon yang bekerja.

    FILE *weapons[150];
    FILE *characters[150];

    getDir(weapons, "./weapons/");
    getDir(characters, "./characters/");

    unsigned int gachaCounter = 0;
    int primogems = 79000;
    char *tempFileString[10];
    short tempFileStringArrCounter = 0;
    time_t startTime = time(NULL);

    int weaponFileTotal = countFiles("./weapons/");
    int characterFileTotal = countFiles("./characters/");

    for (int i = 0; i < 10; i++)
    {
        tempFileString[i] = (char *)malloc(sizeof(char) * 100);
    }

    while (1)
    {
        time_t currTime = time(NULL);

        if (primogems <= gachaCost || (startTime + 10800) < currTime)
        {
            // Akan berjalan ketika primogems habis OR waktu sudah mencapai 3 jam.
            gachaZip();
            break;
        }
        else
        {
            gachaAlgorithm(characters, weapons, &gachaCounter, &primogems, tempFileString, currTime, &tempFileStringArrCounter, weaponFileTotal, characterFileTotal);
        }

        sleep(1);
    }

    for (int i = 0; i < 10; i++)
    {
        free(tempFileString[i]);
    }

    rmFile("characters");
    rmFile("weapons");
    rmDir("characters");
    rmDir("weapons");

    return 0;

}
